#!/bin/bash
NAME="mastermind-gunicorn"                  # Name of the application
PROJECTDIR=/workspace/mastermind/           # Project base directory
NUM_WORKERS=3                               # Gunicorn spawned workers
DJANGO_WSGI_MODULE=mastermind.wsgi
DJANGO_SETTINGS_MODULE=mastermind.settings

echo "Starting $NAME as `whoami`"

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$PROJECTDIR

exec /usr/local/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --bind 0.0.0.0:8000 \

# Project structure
The project folders and files are organized following the Django common structure. The other files or structures added to the project are explained in this section.

## Continuous code quality support files
These files should be located at the root of the project.

- **.coveragerc:** Used to configure unitary test coverage analysis.
- **.pytest.ini:** Used to configure unitary test location and options.
- **.pylintrc:** Used to configure violations policy for the project.

> Any modification of these files should be approved by a reviewer.

## Standard project documentation
The basic project documentation should be versioned and stored at the root of the project. The mandatory files are **CHANGELOG.md**, **CONTRIBUTING.md**, **LICENSE** and **README.md**. If required, some additional documentation files can be added to the project when required.

> The embedded documentation should be written in [Markdown](https://en.wikipedia.org/wiki/Markdown), an standard markup language for plain text formatting. This is indicated with the extension .md. The **LICENSE** file is plain text so avoid the extension in this specific case.

## support/
This folder should contain templates or any other material useful for the development process. `It must not be distributed`.

# Under development distribution
This option should be used only for development purposes.

- *Clone* the repository of the project in your local workspace.
- *Switch/Checkout* to **development** or the appropiate feature branch.
- Open a console, go to the workspace folder and run:

```  
pip install -r requirements.txt
python manage.py migrate
```

At this point you should be able to run an instance of the service in the port 8000 running:

```
python manage.py runserver
```

# Continuous code quality
## Violations
The code violations must be checked using [PyLint](https://www.pylint.org/). In order to perform code violations in the local machine PyLint should be installed:

    pip install -U pylint
    pip install -U pylint_django

PyLint checker rules can be configured using **.pylintrc** file stored at the root of the project. This file configures at project level the rules applied when processing the code violations. This file must be used when the CI is configured and it is the recommended way to check the code in the developer local machine.

> Modifications in the .pylintrc are not permitted without the approval of the project reviewer as this file changes the whole project violations policy.

To evaluate your code using PyLint open a console, go to the root of your project and run:

    pylint -f parseable --rcfile=.pylintrc codemaker

This command provides a detailed report of all the violations in your project and a final evaluation based on these violations. Try to keep this *Global evaluation* result as high as possible and never below the **8.0**.

Also the violations are categorized by its severity, the reviewer must consider the following regarding each violation:

- **\(C\) convention, for programming standard violation**
- **\(R\) refactor, for bad code smell**
- **(W) warning, for python specific problems:** Should be fixed or justified.
- **(E) error, for much probably bugs in the code:** Should be fixed before accept a request.

Pylint violations evaluation criteria:

Target | Optimal | Inadequate | Poor | Unstable
------ | ------- | ---------- | ---- | --------
Pylint violations | 0 - 100 | 100 - 200 | 200 - 1000 | 1000 or more

> Almost all the available IDE provide a way to enable PyLint as code checker. This guide does not provide detailed instructions about how tho configure specific IDE to work with PyLint but it is strongly recommended to use it as the CI evaluation will be done using it.

## Unitary testing
The unitary tests should be executed, at least, each time before make a push to ensure that anything still works as expected. The recommended way to launch the unitary tests is using [py.test](https://docs.pytest.org/en/latest/) and [pytest-cov](https://pytest-cov.readthedocs.io/en/latest/) tools combined.

To run the unitary tests install:

    pip install mock
    pip install pytest
    pip install pytest-cov
    pip install pytest-django

And run:

    py.test -vv --cov=codemaker --cov-config .coveragerc --cov-report term --cov-report html:reports/htmlcov/

The previous command generates a coverage report in the **htmlcov/** folder and executes all the unittests of the project. Check **htmlcov/index.html** for a full coverage report.

The whole project coverage, calculated excluding unitarty tests modules, should be kept as high as possible and should not be lower than **80%**. It is possible to exclude some modules to coverage metrics but it should be discussed and aproved with the reviewer.

These are the unitary testing coverage quality T&V standards:

Target | Optimal | Inadequate | Poor | Unstable
------ | ------- | ---------- | ---- | --------
Packages | 100% - 90.0% | 90% - 60.0% | 60% - 20.0% | 20% - 0%
Files | 100.0% - 90.0% | 90% - 60.0% | 60% - 20.0% | 20% - 0%
Methods | 100.0% - 80.0% | 80.0% - 40.0% | 40.0% - 10.0% | 10.0% - 0%
Lines | 100.0% - 80.0% | 80.0% - 40.0% | 40.0% - 10.0% | 10.0% - 0%
Conditionals | 100.0% - 80.0% | 80.0% - 40.0% | 40.0% - 10.0% | 10.0% - 0%

> The py.test command generates multiple report files that should not be tracked in git, by default they are added to .gitignore file but be carefull to not add in the next commit the htmlcov, .cache, .egg-info or any other support or report file generated by the py.test.

### Unitary tests location and format
Each python source module, .py files, should have a dedicated unitary test python module. The right placement for the unitary test file is a folder that should be named **tests/** at the same level of each django app. Inside the folder the unitary test file should be named as **test\_[module under test name].py**, i.e:

    project-folder/
        app_folder/
          tests/
              __init__.py
              models_test.py
              views_test.py
              ...
          __init__.py
          views.py
          models.py

The *tests* folder must be a python package, so do not forget to add the \__init\__.py file in any test folder created.

>The naming conventions defined here must be followed in order to enable the py.test detection configured in pytest.ini files. Omitting the naming and structure conventions explained here can lead pytest to a misdetection of unitary tests and at the end can lower the code quality evaluation of the project.

# Inline code documentation
This rules should be applied to all modules inside the project package. By now, this is all python modules except the \_\_init\_\_.py module of the base package of the project.

The documentation should be written following the [Google Python Style Guide](https://github.com/google/styleguide/blob/gh-pages/pyguide.md) standards. Please, take a look to the [Google Style Python Docstrings documentation](http://sphinxcontrib-napoleon.readthedocs.io/en/latest/index.html) before to start generating new code for the project.

# Branching
This project workflow is based in [Gitflow workflow](http://nvie.com/posts/a-successful-git-branching-model/). All branches must be created and merged following the **Gitflow** conventions except the release branch that will not be used for this project because there are not differences between release and production environment.

# Revision process
The revision process are made using the *pull request* mechanism provided by Bitbucket. Any merge to the master branch should be made via a *pull request* and requires at least the approval of 1 authorized reviewer.

## Version numbers
This project uses [Sequence-based identifiers](https://en.wikipedia.org/wiki/Software_versioning) as software versioning. In subsequent releases, the **major** number is increased when there are significant jumps in functionality such as changing the framework which could cause incompatibility with interfacing systems, the **minor** number is incremented when only minor features or significant fixes have been added, and the **revision** number is incremented when minor bugs are fixed. `The format of each tag is v[major].[minor].[release]`. e.g:

    v1.3.5

The major is 1, the minor is 3 and release is 5. Each release tag must be created from **master** branch and must match this format.

## Review checklist
- No unauthorized modifications in the quality files: **.coveragerc**, **.pylintrc**, **pytest.ini**.
- The **CHANGELOG.md** explains the modifications in human language for the new release.
- Evaluate if new dependencies have been added and check if the the **requirements.txt** has been updated.
- Last build passes.
- Evaluate the differences.

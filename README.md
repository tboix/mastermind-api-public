# Mastermind Codemaker
The **Mastermind Codemaker** project implements a [RESTful](https://en.wikipedia.org/wiki/Representational_state_transfer) API to simulate the role of the codemaker in the [Mastermind](https://en.wikipedia.org/wiki/Mastermind_(board_game) game. Please read the CONTRIBUTING.md file for more information about the project structure and policies applied in the development process.

## Author
Pull requests reviewer and author of the project:

- Antoni Boix antoni.boix.requesens@gmail.com

## Design considerations
The solution has been implemented in pure Django. This decision has been taken because of project restrictions. The possibility to use [django-rest-framework](https://www.django-rest-framework.org/) or [flask-restful](https://flask-restful.readthedocs.io/en/latest/) (the option I feel more confortable with for these type of implementations) have been discarted for this reason.

## Run in production
The project is Docker ready. It can be distributed as a Docker image.

To build a docker image run:

```
docker build -t mastermind-21b .
```

To execute a previously build image:

```
docker run -p 0.0.0.0:8000:8000 --name m21b -d mastermind-21b
```

In order to stop/start the container run:

```
docker stop m21b
docker start m21b
```

> The development mode is explained in the CONTRIBUTING.md.

# REST documentation

> Some of the implemented methods are not documented yet. Use the [postman](https://www.getpostman.com/) collection included in the support folder to get more information of all the available services.

## /codemaker/
Available codemaker service methods.

- `POST` codemaker/
- `POST` codemaker/:game_id/break/
- `GET`  codemaker/:game_id/history/
- `GET`  codemaker/:game_id/history/moves/
- `GET`  codemaker/board/history/
- `GET`  codemaker/board/:game_id/

### POST codemaker/
Start a new game.

Request: `POST` `application/json`

Parameter | Mandatoriness | Type | Description
--------- | ------------- | ---- | -----------
- | - | - | -

### Example
#### Request

GET http://localhost:8000/codemaker/

#### Response

```
{
    "game_id": "c147ac52-b412-4e7b-a8bf-cca6f0f60409"
}
```

The game_id returned shall be used in future queries to identify this specific game.

> The game_id is used to provide support for multiple ongoing games at the same time.

### POST codemaker/:game_id/break/
Try to guess the codemaker code, it should be used after starting a new game and the game_id of the created game must be provided.

Request: `POST` `application/json`

Parameter | Mandatoriness | Type | Description
--------- | ------------- | ---- | -----------
:game_id | `REQUIRED` | `string` | Game public unique identifier
code | `REQUIRED` | `array` | Array of strings containing the code guessed by the codebreaker. The allowed colors are *green*, *blue*, *red*, *purple* and *yellow*.

### Example
#### Request
GET http://localhost:8000/codemaker/c147ac52-b412-4e7b-a8bf-cca6f0f60409/break/

```
{
    "code": ["red", "blue", "purple", "red"]
}
```

##### Response

```
{
    "game_id": "c147ac52-b412-4e7b-a8bf-cca6f0f60409",
    "move": {
        "current": 1,
        "pending": 11
    },
    "pegs": {
        "red": 1,
        "white": 1
    },
    "state": "ongoing"
}
```

The **state** field indicates the state of the game. The state can be *ongoing*, *victory* or *defeat*.
The **pegs** field indicates the mastermind key pegs returned by the codemaker for the provided sequence.

## Todo
Some points are still open:
- Logs
- Authentication for management services
- Improve unitary testing
- Complete the API user documentation
- Complet some missing inline documentation in models.py
- Add CI/CD process

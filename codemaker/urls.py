"""
Codemaker application urls file.

Author:
    Antoni Boix <antoni.boix.requesens@gmail.com>

Note:
    C0103 pylint policy disabled locally due to Django naming conventions.
"""

#pylint:disable=C0103

from django.conf.urls import url

from . import views

urlpatterns = [
    # Board/management endpoints
    url(
        r"^board/history/$", views.board_history_games,
        name="board_history_games"
    ),
    url(
        r"^board/(?P<game_id>.+)/$",
        views.board_history_game, name="board_history_game"
    ),

    url(r'^$', views.start, name="start"),
    url(r"^(?P<game_id>.+)/break/$", views.codebreak, name="break"),
    url(
        r"^(?P<game_id>.+)/history/$", views.history_game,
        name="history_game"
    ),
    url(
        r"^(?P<game_id>.+)/history/moves/$", views.history_move,
        name="history_move"
    )
]

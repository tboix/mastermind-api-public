"""
Unitary tests for models module

Author:
    Antoni Boix <antoni.boix.requesens@gmail.com>

Note:
    Relaxed pylint directives in unitary testing modules.
"""

# pylint:disable=C0103,C0111,W0212,W0611

import json
import logging

from django import test
import mock

from codemaker import models

class TestCode(test.TestCase):
    """
    Unitary tests for models.Code class.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)
        cls.code_seq = ["green", "red", "purple", "yellow"]
        cls.code_seq_bad_len = ["green"]
        cls.code_seq_bad_color = ["green", "red", "purple", "black"]
        cls.code_seq_test = ["green", "yellow", "green", "purple"]

    def setUp(self):
        """
        Test setUp.
        """

        self.code_i = models.Code(self.code_seq)

    def test_raise_on_invalid_code_on_length_err(self):
        with self.assertRaises(models.InvalidCodeLengthError):
            models.Code.raise_on_invalid_code(self.code_seq_bad_len)

    def test_raise_on_invalid_code_on_color_err(self):
        with self.assertRaises(models.InvalidCodeColorError):
            models.Code.raise_on_invalid_code(self.code_seq_bad_color)

    def test_build_random_code(self):
        result = models.Code.build_random_code()
        self.assertIsInstance(result, models.Code)
        self.assertEqual(len(result.code), models.Code.CODE_LENGTH)

    def test_build_from_list(self):
        result = models.Code.build_from_list(self.code_seq)
        self.assertIsInstance(result, models.Code)

    def test_build_from_str(self):
        result = models.Code.build_from_str(json.dumps(self.code_seq))
        self.assertIsInstance(result, models.Code)

    def test_dump(self):
        result = self.code_i.dump()
        self.assertIsInstance(result, str)

    def test_get_keypegs(self):
        result = self.code_i.get_keypegs(self.code_seq_test)
        self.assertIsInstance(result, models.Keypegs)
        self.assertEqual(result.red, 1)
        self.assertEqual(result.white, 2)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

"""
Unitary tests for views module.

Author:
    Antoni Boix <antoni.boix.requesens@gmail.com>

Note:
    Relaxed pylint directives in unitary testing modules.
"""

# pylint:disable=C0103,C0111,W0212,W0611

import json
import logging

from django import test
from django.urls import reverse
import mock

from codemaker import models
from codemaker import views

class CodebreakView(test.TestCase):
    """
    Unitary tests for codemaker:break view.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

    def setUp(self):
        """
        Test setUp.
        """

    def test_on_bad_format(self):
        response = self.client.post(
            path=reverse('codemaker:break', kwargs={
                "game_id": "InvalidGameId",
            }),
            data="This is not a valid JSON",
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 400)

    def test_on_bad_schema(self):
        response = self.client.post(
            path=reverse('codemaker:break', kwargs={
                "game_id": "InvalidGameId",
            }),
            data={
                "bad": "schema"
            },
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 400)

    @mock.patch("codemaker.models.Game.objects.get")
    def test_on_unknown_game_id(self, game_get_mock):
        game_get_mock.side_effect = models.Game.DoesNotExist
        game_id = "a-game-id"

        response = self.client.post(
            path=reverse('codemaker:break', kwargs={
                "game_id": game_id,
            }),
            data={
                "code": ["green", "green", "green", "green"]
            },
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 404)
        game_get_mock.assert_called_once_with(public_id=game_id)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

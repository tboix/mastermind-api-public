"""
Codemaker views. This module includes the exposed views implementation for
the codemaker functionality including the management associated resources.

Some of the game logic like the decision, the victory trigger, it is also
included in this module.

Author:
    Antoni Boix <antoni.boix.requesens@gmail.com>

Note:
    The documentation of the request argument can be ignored in all the
    functions included in this module as a standard part of Django framework.
"""

import json
import enum

import jsonschema

from django.http import JsonResponse
from django.views.decorators.http import require_POST, require_GET

from codemaker import models

CODEBREAK_SCHEMA = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Probe message format",
    "type": "object",
    "additionalProperties": False,
    "properties": {
        "code": {
            "type": "array",
            "items": {
                "type": "string",
                "enum": ["yellow", "blue", "purple", "green", "red"]
            },
            "minItems": 4,
            "maxItems": 4,
        }
    }
}

class GameState(enum.Enum):
    """
    Enumeration of game states.
    """

    ONGOING = "ongoing"
    DEFEAT = "defeat"
    VICTORY = "victory"


def default_error(err_msg, status_code=400):
    """
    Default error response builder.

    Args:
        err_msg (str): Error message returned in the error response.
        status_code (int, optional): Status code returned in the HTTP
            response.
    """

    return JsonResponse({
        "error": err_msg,
    }, status=status_code)

def handler404(request, *args, **argv):
    """
    Reimplementation of default Django 404 error handler.
    """

    return default_error("Resource not found", status_code=404)

def handler500(request, *args, **argv):
    """
    Reimplementation of default Django 404 error handler.
    """

    return default_error("Unexpected internal server error", status_code=500)


@require_POST
def start(request):
    """
    Start a new game.
    """

    new_game = models.Game.create_game()
    new_game.save()
    print(new_game.code)
    return JsonResponse({
        "game_id": new_game.public_id,
    })

@require_POST
def codebreak(request, game_id):
    """
    Attempt to break the code of ongoing game.

    Args:
        game_id (str): Unieque public game identifier.

    Note:
        Some of the code is not reachable, basically the CodemakerModelError
        handling because is previously protected by the schema validation. I
        keep the code to show how I would generate errors based on model
        errors using custom exceptions grouped by exception inheritance.
    """

    try:
        codebreak_body = json.loads(request.body)
    except json.decoder.JSONDecodeError as err:
        return default_error("Invalid JSON body")

    try:
        jsonschema.validate(codebreak_body, CODEBREAK_SCHEMA)
    except jsonschema.ValidationError as err:
        return default_error(str(err))

    try:
        game = models.Game.objects.get(public_id=game_id)
    except models.Game.DoesNotExist:
        return default_error(
            "Unknown game: {}".format(game_id), status_code=404
        )

    if not game.ongoing:
        return default_error("Inactive game")

    state = GameState.ONGOING.value
    move = models.Move()
    move.game = game

    try:
        move.set_guess(codebreak_body["code"])
        move.save()
        move_count = models.Move.objects.filter(game=game).count()
        feedback = game.get_feedback(codebreak_body["code"])
    except models.CodemakerModelError as err:
        return default_error(str(err))

    if feedback.red == models.Code.CODE_LENGTH:
        game.exposed = True
        game.ongoing = False
        state = GameState.VICTORY.value
    else:
        if move_count >= models.MAX_MOVES:
            game.ongoing = False
            state = GameState.DEFEAT.value

    game.save()

    return JsonResponse({
        "game_id": game_id,
        "move": {
            "current": move_count,
            "pending": models.MAX_MOVES - move_count,
        },
        "pegs": {
            "red": feedback.red,
            "white": feedback.white,
        },
        "state": state,
    })

@require_GET
def history_game(request, game_id):
    """
    Retrieve the state of a specific game. The codemaker secret code is not
    provided.

    Args:
        game_id (str): Unieque public game identifier.
    """

    try:
        game = models.Game.objects.get(public_id=game_id)
    except models.Game.DoesNotExist:
        return default_error(
            "Unknown game: {}".format(game_id), status_code=404
        )
    moves = models.Move.objects.filter(game=game)

    return JsonResponse({
        "game_id": game_id,
        "move": {
            "current": moves.count(),
            "pending": models.MAX_MOVES - moves.count(),
        },
        "ongoing": game.ongoing,
        "exposed":  game.exposed,
        "moves": [move.get_guess() for move in moves],
    })

@require_GET
def history_move(request, game_id):
    """
    Retrieve the break attempts history of a specific game.

    Args:
        game_id (str): Unieque public game identifier.
    """

    try:
        game = models.Game.objects.get(public_id=game_id)
    except models.Game.DoesNotExist:
        return default_error(
            "Unknown game: {}".format(game_id), status_code=404
        )
    moves = models.Move.objects.filter(game=game)

    return JsonResponse({
        "moves": [move.get_guess() for move in moves]
    })

# TODO: Protect the access to this method with an auth mechanism (Board only)
# TODO: Pagination may be required
@require_GET
def board_history_games(request):
    """
    Retrieve the public identifier for all games.
    """

    games = models.Game.objects.all()
    return JsonResponse({
        "games": [game.public_id for game in games]
    })

# TODO: Protect the access to this method with an auth mechanism (Board only)
@require_GET
def board_history_game(request, game_id):
    """
    Retrieve all the information regarding a specific game including the
    codemaker secret code.

    Args:
        game_id (str): Unieque public game identifier.
    """

    try:
        game = models.Game.objects.get(public_id=game_id)
    except models.Game.DoesNotExist:
        return default_error(
            "Unknown game: {}".format(game_id), status_code=404
        )
    moves = models.Move.objects.filter(game=game)

    return JsonResponse({
        "game_id": game_id,
        "move": {
            "current": moves.count(),
            "pending": models.MAX_MOVES - moves.count(),
        },
        "code": game.get_code(),
        "ongoing": game.ongoing,
        "exposed":  game.exposed,
        "moves": [move.get_guess() for move in moves],
    })

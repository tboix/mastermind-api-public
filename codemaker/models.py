"""
Codemaker models and codemaker code processing game logic.

Author:
    Antoni Boix <antoni.boix.requesens@gmail.com>
"""

import json
import uuid
import random
import collections

from django.db import models

class CodemakerModelError(Exception):
    """
    Base model application level errors for codemaker models.
    """

class InvalidCodeColorError(CodemakerModelError):
    """
    Raised when an invalid color is provided as part of the code.
    """

class InvalidCodeLengthError(CodemakerModelError):
    """
    Raised when the length of the code does not match the expected.
    """


class Code():
    """
    Support class to process mastermind codes. This is encode/decode from and
    to the required formats, integrity checks and keypegs calculation.
    """

    YELLOW = "yellow"
    BLUE = "blue"
    PURPLE = "purple"
    GREEN = "green"
    RED = "red"

    CODE_PEGS_CHOICES = (
        (YELLOW, "yellow"),
        (BLUE, "blue"),
        (PURPLE, "purple"),
        (GREEN, "green"),
        (RED, "red"),
    )

    CODE_PEGS_VALID = [color for color, _ in CODE_PEGS_CHOICES]
    CODE_LENGTH = 4

    def __init__(self, code):
        self.code = code

    @classmethod
    def raise_on_invalid_code(cls, code):
        """
        """

        if len(code) != cls.CODE_LENGTH:
            raise InvalidCodeLengthError(
                "Unexpected code length, {}".format(len(code))
            )

        for color in code:
            if color not in cls.CODE_PEGS_VALID:
                raise InvalidCodeColorError((
                    "Unexpected code color {color}. "
                    "Allowed color values are {choices}"
                ).format(color=color, choices=cls.CODE_PEGS_VALID))


    @classmethod
    def build_random_code(cls):
        """
        """

        return cls(random.choices(cls.CODE_PEGS_VALID, k=cls.CODE_LENGTH))

    @classmethod
    def build_from_list(cls, code):
        """
        """

        cls.raise_on_invalid_code(code)
        return cls(code)

    @classmethod
    def build_from_str(cls, strcode):
        """
        """

        code = json.loads(strcode)
        cls.raise_on_invalid_code(code)
        return cls(code)

    def dump(self):
        """
        """

        return json.dumps(self.code)

    def get_keypegs(self, guess_code):
        """
        Provide feedback for a codebreaker guess against the stored code.

        Args:
            guess_code (list): List of strings where each string should be a
                valid code peg value.

        Returns:
            namedtuple: two elements namedtuple with the key pegs assigned to
                the given guess.
        """

        guess_code = Code.build_from_list(guess_code).code

        rp_colors = []
        wp_colors = []
        red_pegs = 0

        for indx, guess_element in enumerate(guess_code):

            if guess_element == self.code[indx]:
                red_pegs += 1

                if guess_element not in rp_colors:
                    rp_colors.append(guess_element)

                if guess_element in wp_colors:
                    wp_colors.remove(guess_element)

            elif guess_element in self.code:
                if guess_element not in rp_colors and \
                        guess_element not in wp_colors:
                    wp_colors.append(guess_element)

        return Keypegs(red_pegs, len(wp_colors))


MAX_MOVES = 12
Keypegs = collections.namedtuple("Keypegs", ("red", "white"))


class Game(models.Model):
    """
    Game model.
    """

    public_id = models.CharField(
        max_length=40, unique=True, help_text="Public identifier"
    )
    code = models.CharField(
        max_length=100, help_text="Codemaker code"
    )
    ongoing = models.BooleanField(default=True)
    exposed = models.BooleanField(default=False)

    @classmethod
    def create_game(cls):
        """
        """

        return cls(
            public_id=str(uuid.uuid4()),
            code=Code.build_random_code().dump()
        )

    def get_code(self):
        """
        """

        return Code.build_from_str(self.code).code

    def get_feedback(self, codebreaker_guess):
        """
        """

        return Code.build_from_str(self.code).get_keypegs(codebreaker_guess)


class Move(models.Model):
    """
    Code guess model. Code guess history.
    """

    game = models.ForeignKey(
        Game, on_delete=models.CASCADE, null=False, blank=False
    )
    guess = models.CharField(
        max_length=100, help_text="Codebreaker guess", null=False, blank=False
    )

    def get_guess(self):
        """
        """

        return Code.build_from_str(self.guess).code

    def set_guess(self, codebreaker_guess):
        """
        """

        self.guess = Code.build_from_list(codebreaker_guess).dump()

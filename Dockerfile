FROM python:3.7

WORKDIR /workspace/mastermind

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN pip install gunicorn

COPY mastermind_gunicorn.sh mastermind_gunicorn.sh
COPY manage.py manage.py
COPY ./mastermind ./mastermind
COPY ./codemaker ./codemaker

ENV PYTHONPATH /workspace/mastermind
RUN python manage.py migrate

EXPOSE 8000

CMD bash mastermind_gunicorn.sh
